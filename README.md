# Examen Segunda Evaluación Diseño Web y Desarrollo Cliente

# Día 17/02/2022 Tiempo: 5 horas

- Nota: Cada pregunta se valorará como bien o como mal (valoraciones intermedias serán excepcionales).
- Nota2: En cada pregunta se especifica si se valora en el examen de diseño o en el de desarrollo.
- Nota3: Para aprobar cada examen hay que obtener una puntuación mínima de 5 puntos en ese examen.
- Nota4: Organice su tiempo. Si no consigue resolver un apartado pase al siguiente. El examen consta de apartados de diseño y de desarrollo que se pueden resolver por separado. Si un apartado depende de otro que no sabe resolver, siempre puede dar una solución que aunque no sea correcta, le permita seguir avanzando.
- Nota5: Para que una solución sea correcta, no sólo hay que conseguir que haga lo que se pide, sino que además **todo lo que funcionaba lo tiene que seguir haciendo**. La solución debe estar implementada según las prácticas de código limpio explicadas en clase. Esto incluye JavaScript, CSS y HTML.
- Nota6: Lea completamente el examen antes de empezar y comience por lo que le parezca más fácil.
- Nota7: No se permite utilizar ninguna librería que no esté ya incluida en el fichero "package.json".
- Nota8: Debe implementar la solución en los ficheros que se le proporcionan.
- Nota9: No debe haber "warnings" ni errores en la consola.

Pasos previos antes de empezar

- Clone el repositorio del enunciado

```bash
git clone https://gitlab.com/surtich/enunciado-examen-segunda-2021-2022.git enunciado-examen-segunda
```

- Vaya al directorio del proyecto

```bash
cd enunciado-examen-segunda
```

- Configure su usuario de git:

```bash
git config user.name "Sustituya por su nombre y apellidos"
git config user.email "Sustituya por su correo electrónico"
```

- Cree un _branch_ con su nombre y apellidos separados con guiones (no incluya mayúsculas, acentos o caracteres no alfabéticos, excepción hecha de los guiones). Ejemplo:

```bash
    git checkout -b fulanito-perez-gomez
```

- Compruebe que está en la rama correcta:

```bash
    git status
```

- Suba la rama al repositorio remoto:

```bash
    git push origin <nombre-de-la-rama-dado-anteriormente>
```

- Arrange el servidor:

```bash
    yarn server
```

- Compruebe que suministra datos [http://localhost:3002/books](http://localhost:3002/books):

- En otro terminal, instale las dependencias y arranque el cliente:

```bash
    yarn install
    yarn start
```

Navegue a [http://localhost:3000](http://localhost:3000)


Debería ver esto:

![Image 0](./img/0.png)


- Dígale al profesor que ya ha terminado para que compruebe que todo es correcto y desconecte la red.

## OBJETIVO

Se pretende clasificar una serie de libros en tres listas: leyendo, finalizados, descubrimiento.

![Video 0](./img/0.gif)


### 1.- Barra de navegación.

El resultado final será.

![Image 1](./img/1.png)

#### 1.1.- (0,5 puntos diseño) La barra de navegación (componente Nav) y los libros (componente Book) estarán uno al lado del otro.

#### 1.2.- (1 punto diseño) La barra de navegación tendrá el estilo que se muestra: 

![Image 1.2](./img/1.2.gif)

#### 1.3.- (1 punto desarrollo) Al pulsar cambiará la opción seleccionada. 

![Image 1.3](./img/1.3.gif)

Nota Importante: Para evitar que se recargue la página debe incluir esto en el manejador del evento "click" de los enlaces (etiquetas <a>).

```javascript
function handleClick(e) {
    e.nativeEvent.preventDefault();
    // resto del código
}

```

#### 1.4.- (2 puntos diseño) Los libros tendrán el estilo mostrado y estarán alineados con a la barra de navegación. 

![Image 1.4](./img/1.4.png)

#### 1.5.- (1 punto diseño) La barra estará anclada en la parte superior. 

![Image 1.5](./img/1.5.gif)

#### 1.6.- (0,5 puntos diseño) La aplicación estará centrada en el navegador. 


#### 1.7.- (1 punto desarrollo) Los libros sólo se verán en la ventana de descubrimiento. Con el resto de opciones se verán los textos mostrados.  

![Image 1.7](./img/1.7.gif)

Nota: El texto para la opción leyendo es:

```html
<p>Hey there! Welcome to your bookshelf reading list. Get started by heading over to <a href="/">the Discover page</a> to add books to your list.</p>
```

Nota: El texto para la opción finalizados es:

```html
<p>Hey there! This is where books will go when you've finished reading them. Get started by heading over to <a href="/">the Discover page</a> to add books to your list.</p>
```

#### 1.8.- (0,5 puntos desarrollo) Al pulsar sobre el enlace "the Discover page" se seleccionará la página de descubrimiento. 

![Image 1.8](./img/1.8.gif)


### 2.- Comportamiento "responsivo".

#### 2.1.- (1 punto diseño) Los libros se adaptarán al tamaño de la pantalla. 

![Image 2.1](./img/2.1.gif)

#### 2.2.- (2 puntos diseño) Añada un punto de interrupción según el vídeo que se muestra a continuación. Sea muy cuidadoso con márgenes y alineaciones. 

![Image 2.2](./img/2.2.gif)

### 3. Carga de libros.

![Image 3](./img/3.gif)

### 3.1.- (1 punto desarrollo) Utilice el fichero "api/books.js" y el fichero "reducer/book.js" para cargar los libros. Debe modificar el componente Book (fichero "components/Book.js") para que se muestren los datos recuperados.

Nota: En los ficheros se dan las funciones que necesita ya implementadas aunque las puede modificar si lo cree conveniente.

Nota: Si no supiera hacer la carga asíncrona siempre podría leer el fichero "db.json" directamente en el componente App (se penalizaría hacer eso con -0,5 puntos).


### 3.2.- (1 punto desarrollo) La carga de datos se producirá la primera vez que navegue a la página de descubrimiento. Una vez cargados los libros, ya no se volverán a cargar.

Nota: Es decir, que los libros no se cargarán al principio sino cuando se navegue a descubrimiento. Las sucesivas veces que se navegue a descubrimiento ya no se volverán a cargar los libros.

### 3.3.- (0,5 puntos desarrollo) Durante la carga se verá el componente Spinner y los siguientes textos delante del Spinner:

```html
<p>Welcome to the discover page.</p>
<p>Here, let me load a few books for you...</p>
```
### 3.4.- (1 punto diseño) El componente Spinner estará centrado.

Nota: Si no hace la parte de desarrollo, asegúrese de que se muestra siempre el Spinner.
### 4.- Gestión de listas (leyendo, finalizados, descubrimiento):

![Image 4](./img/4.gif)

### 4.1 (1 punto desarrollo) En descubrimiento sólo aparecerá un botón (+). Al pulsar sobre +, el libro se pasará a la lista leyendo.

Nota: Esto se hará en el cliente modificando el reductor (preferiblemente) o con estados.

Nota: En este apartado y en los siguientes se penalizará la repetición innecesaria de código.

Consejo: Implemente primero todos los apartados y luego refactorice para reducir la repetición de código.

Consejo: Aunque se habla de tres listas, probablemente le resulte más sencillo tener un lista de libros y diferenciar con un atributo el estado en que se encuentra cada libro.
### 4.2 (1 punto diseño) El botón + estará centrado.

Nota: Si no sabe ocultar dinámicamente el botón -, muestre al menos de forma estática dos libros (uno con dos botones + y -) y otro con un sólo un botón +.

### 4.3 (1 punto desarrollo) Si hay libros en la lista leyendo se mostrarán los libros en lugar del texto. Los libros tendrán dos botones (+ y -).


### 4.4 (1 punto desarrollo) En la lista leyendo si se pulsa sobre + el libro pasará a la lista finalizados; y si se pulsa sobre - el libro volverá a la lista de descubrimiento.

### 4.5 (1 punto desarrollo) Si hay libros en la lista finalizados se mostrarán los libros en lugar del texto. Los libros tendrán dos botones (read y -).

### 4.6 (1 punto desarrollo) En la lista finalizados, si se pulsa sobre "read" el libro pasará a la lista leyendo; y si se pulsa sobre - el libro volverá a la lista de descubrimiento.

## Para entregar

- Ejecute el siguiente comando para comprobar que está en la rama correcta y ver los ficheros que ha cambiado:

```bash
    git status
```

- Prepare los cambios para que se añadan al repositorio local:

```bash
    git add .
    git commit -m "completed exam"
```

- Compruebe que no tiene más cambios que incluir:

```bash
    git status
```

- Dígale al profesor que va a entregar el examen.

- Conecte la red y ejecute el siguiente comando:

```bash
    git push origin <nombre-de-la-rama>
```

- Abandone el aula en silencio.