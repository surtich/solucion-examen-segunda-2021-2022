const initialBooksState = {
  loading: false,
  books: [],
};

const bookReducer = (state, action) => {
  switch (action.type) {
    case "BOOK_FETCH_INIT":
      return {
        ...state,
        loading: true,
      };
    case "BOOK_FETCH_SUCCESS":
      return {
        loading: false,
        books: action.payload.books,
      };
    case "BOOK_ADD_READING":
      return {
        ...state,
        books: state.books.map((book) =>
          book.id === action.payload.bookId
            ? { ...book, reading: action.payload.reading, finished: false }
            : { ...book }
        ),
      };
    case "BOOK_ADD_FINISHED":
      return {
        ...state,
        books: state.books.map((book) =>
          book.id === action.payload.bookId
            ? { ...book, finished: action.payload.finished, reading: false }
            : { ...book }
        ),
      };
    default:
      throw Error("Action Books Error!");
  }
};

export { initialBooksState, bookReducer };
