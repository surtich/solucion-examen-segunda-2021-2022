import { Spinner } from "./Spinner";

export default function Text({ navOption, loading, gotoDiscover }) {
  const text = (t) => (
    <p>
      Hey there! {t} Get started by heading over to{" "}
      <a
        onClick={(e) => {
          e.nativeEvent.preventDefault();
          gotoDiscover();
        }}
        href="/"
      >
        the Discover page
      </a>{" "}
      to add books to your list.
    </p>
  );
  switch (navOption) {
    case 0:
      return text("Welcome to your bookshelf reading list.");
    case 1:
      return text(
        "This is where books will go when you've finished reading them."
      );
    case 2:
    default:
      return (
        <div className="text">
          <p>Welcome to the discover page.</p>
          <p>Here, let me load a few books for you...</p>
          <div
            style={{
              textAlign: "center",
            }}
          >
            {loading && (
              <Spinner
                style={{
                  fontSize: "3rem",
                  marginBottom: "1rem",
                }}
              />
            )}
          </div>
        </div>
      );
  }
}
