import { API_ENDPOINT } from "../api/books";
import "./Book.scss";

function Plus() {
  return (
    <svg
      stroke="currentColor"
      fill="currentColor"
      strokeWidth="0"
      viewBox="0 0 512 512"
      height="1em"
      width="1em"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm144 276c0 6.6-5.4 12-12 12h-92v92c0 6.6-5.4 12-12 12h-56c-6.6 0-12-5.4-12-12v-92h-92c-6.6 0-12-5.4-12-12v-56c0-6.6 5.4-12 12-12h92v-92c0-6.6 5.4-12 12-12h56c6.6 0 12 5.4 12 12v92h92c6.6 0 12 5.4 12 12v56z"></path>
    </svg>
  );
}

function Book({}) {
  return (
    <div className="book">
      <div className="book__img">
        <img
          src={`${API_ENDPOINT}/512BxGDb9JYL._SX338_BO1,204,203,200_.jpg`}
          alt="Where the Red Fern Grows book cover"
          className="book__img"
        />
      </div>
      <div className="book__content">
        <div className="book__header">
          <div className="book__title">
            <h2>Where the Red Fern Grows</h2>
          </div>
          <div>
            <div className="book__author">Wilson Rawls</div>
            <small className="book__publisher">Yearling</small>
          </div>
        </div>
        <small className="book__synopsis">
          Billy has long dreamt of owning not one, but two, dogs. So when he’s
          finally able to save up enough money for two pups to call his own—Old
          Dan and Little Ann—he’s ecstatic. It doesn’t matter that times are
          tough; together they’ll roam the hills of the Ozarks. Soon Billy and
          his hounds become the finest hunting team in the valley. Stories of
          their great achievements spread throughout the region, and the
          combination of Old Dan’s brawn, Little Ann’s brains, and Billy’s sheer
          will seems unbeatabl...
        </small>
      </div>
      <div className="book__plus-container">
        <button className="book__plus">
          <Plus />
        </button>
      </div>
    </div>
  );
}

export default Book;
