import { useEffect, useReducer, useState } from "react";
import Nav from "./Nav";
import "./App.scss";
import { bookReducer, initialBooksState } from "../reducers/book";
import { loadBooks } from "../api/books";
import Book from "./Book";
import Text from "./Text";

export default function App() {
  const [navOption, setNavOption] = useState(0);
  const [bookState, bookDispatch] = useReducer(bookReducer, initialBooksState);
  useEffect(
    function () {
      if (navOption === 2 && bookState.books.length === 0) {
        bookDispatch({
          type: "BOOK_FETCH_INIT",
        });
        loadBooks().then(function ({ books }) {
          bookDispatch({
            type: "BOOK_FETCH_SUCCESS",
            payload: {
              books,
            },
          });
        });
      }
    },
    [navOption, bookState.books.length]
  );

  function addToReading(bookId, reading = true) {
    return function () {
      bookDispatch({
        type: "BOOK_ADD_READING",
        payload: {
          bookId,
          reading,
        },
      });
    };
  }

  function addToFinished(bookId, finished = true) {
    return function () {
      bookDispatch({
        type: "BOOK_ADD_FINISHED",
        payload: {
          bookId,
          finished,
        },
      });
    };
  }

  function filterBooks() {
    switch (navOption) {
      case 0:
        return bookState.books
          .filter((book) => book.reading && !book.finished)
          .map((book) => (
            <li key={book.id} style={{ marginBottom: "1rem" }}>
              <Book
                {...book}
                onCheck={addToFinished(book.id)}
                onMinus={addToFinished(book.id, false)}
              />
            </li>
          ));
      case 1:
        return bookState.books
          .filter((book) => book.finished)
          .map((book) => (
            <li key={book.id} style={{ marginBottom: "1rem" }}>
              <Book
                {...book}
                onRead={addToReading(book.id)}
                onMinus={addToReading(book.id, false)}
              />
            </li>
          ));
      case 2:
        return bookState.books
          .filter((book) => !book.reading && !book.finished)
          .map((book) => (
            <li key={book.id} style={{ marginBottom: "1rem" }}>
              <Book {...book} onPlus={addToReading(book.id)} />
            </li>
          ));
      default:
        return [];
    }
  }

  const books = filterBooks();

  return (
    <div className="app">
      <Nav navOption={navOption} setNavOption={setNavOption} />
      <div className="body">
        {books.length === 0 && (
          <Text
            navOption={navOption}
            loading={bookState.loading}
            gotoDiscover={() => setNavOption(2)}
          />
        )}
        <ul style={{ marginLeft: "1rem", listStyle: "none" }}>{books}</ul>
      </div>
    </div>
  );
}
